@extends('layouts.app')

    @section('content')
        <h1>Tickets</h1>
        <a href="{{route('tickets.create')}}" class="btn btn-primary">Log new ticket</a>
        @if(count($tickets))
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Customer name</th>
                <th scope="col">Title</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tickets as $ticket)
            <tr>
                <td><a href="/tickets/{{$ticket->id}}">{{$ticket->customer_name}}</a></td>
                <td>{{$ticket->title}}</td>
                <td>{{$ticket->status['name']}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
            {{$tickets->links()}}
            @else
            <p>There is no tickets!</p>
        @endif
    @endsection