@extends('layouts.app')

@section('content')
    <h1>Add new ticket</h1>
    {!! Form::open(['action' => 'TicketsController@store'], ['class' => 'form-group']) !!}
    {{ Form::text('customer_name', '',['class' => 'form-control', 'placeholder' => 'Customer name']) }}
    {{ Form::email('email', '',['class' => 'form-control', 'placeholder' => 'Email']) }}
    {{ Form::text('phone_number', '',['class' => 'form-control', 'placeholder' => 'Phone number']) }}
    {{ Form::text('title', '',['class' => 'form-control', 'placeholder' => 'Title']) }}
    {{ Form::textarea('description', '',['class' => 'form-control', 'placeholder' => 'Description']) }}
    {{Form::select('status', $statuses)}}
    {{ Form::submit() }}
    {!! Form::close() !!}
@endsection