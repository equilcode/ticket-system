@extends('layouts.app')

@section('content')
{!! Form::open(['action' => ['TicketsController@update', $ticket->id]], ['class' => 'form-group']) !!}
{{ Form::text('customer_name', $ticket->customer_name,['class' => 'form-control', 'placeholder' => 'Customer name']) }}
{{ Form::email('email', $ticket->email,['class' => 'form-control', 'placeholder' => 'Email']) }}
{{ Form::text('phone_number', $ticket->phone_number,['class' => 'form-control', 'placeholder' => 'Phone number']) }}
{{ Form::text('title', $ticket->title,['class' => 'form-control', 'placeholder' => 'Title']) }}
{{ Form::textarea('description', $ticket->description,['class' => 'form-control', 'placeholder' => 'Description']) }}
{{Form::select('status', $statuses, $ticket->status_id)}}
{{Form::hidden('_method', 'PATCH')}}
{{ Form::submit('Save', ['class' => 'btn btn-success']) }}

{!! Form::close() !!}
{!! Form::open(['action' => ['TicketsController@closeTicket', $ticket->id]]) !!}
{{ Form::submit('Close',['class' => 'btn btn-danger']) }}
{!! Form::close() !!}
<a class="btn btn-primary" href="{{route('tickets.index')}}">Back</a>
    @endsection