<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function status() {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }
}
