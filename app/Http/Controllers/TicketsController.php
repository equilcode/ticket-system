<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Status;

class TicketsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::orderBy('created_at', 'desc')->paginate(5);
        return view('tickets.index')->with(
            'tickets', $tickets
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();
        $preparedForSelect = [];
        foreach($statuses as $status){
            $preparedForSelect[$status->id] = $status->name;
        }
        return view('tickets.create')->with('statuses', $preparedForSelect);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'customer_name' => 'required',
            'email' => 'required|email',
            'title' => 'required',
            'description' => 'required',
            'phone_number' => 'numeric'
        ]);
        $ticket = new Ticket();
        $ticket->customer_name = $request->customer_name;
        $ticket->email = $request->email;
        $ticket->phone_number = $request->phone_number;
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->status_id = (int)$request->status;
        $ticket->save();
        return redirect('/tickets')->with('success', 'Task created!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::find($id);
        $statuses = Status::all();
        $preparedForSelect = [];
        foreach($statuses as $status){
            $preparedForSelect[$status->id] = $status->name;
        }
        return view('tickets.show')->with([
            'ticket' => $ticket,
            'statuses' => $preparedForSelect
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'customer_name' => 'required',
            'email' => 'required|email',
            'title' => 'required',
            'description' => 'required',
            'phone_number' => 'numeric'
        ]);
        $ticket = Ticket::find($id);
        $ticket->customer_name = $request->customer_name;
        $ticket->email = $request->email;
        $ticket->phone_number = $request->phone_number;
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->status_id = (int)$request->status;
        $ticket->save();
        return redirect('/tickets')->with('success', 'Task updated!');
    }

    public function closeTicket(Request $request, $id) {
        $ticket = Ticket::find($id);
        $ticket->status_id = 3;
        $ticket->save();
        return redirect('/tickets')->with('success', 'Task closed!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
